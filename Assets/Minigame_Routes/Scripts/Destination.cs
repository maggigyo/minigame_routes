﻿using UnityEngine;
using System.Collections;

public class Destination : MonoBehaviour {

	public string name;
	public GameObject flag;
	public static GameManager gamemanager;
	// Use this for initialization
	void Start () {
		gamemanager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		gamemanager.enterDestination (name);

	}

	void OnTriggerExit2D(Collider2D other) {
		gamemanager.exitDestination ();

	}
}
