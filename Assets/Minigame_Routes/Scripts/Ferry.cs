﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Ferry : MonoBehaviour {

	public bool dragging = false;
	public static GameObject map;

	//private Sprite flagsprite;
	private string destination;
	private Vector3 startPoint;

	private static float mapMaxX;
	private static float mapMinX;
	private static float mapMaxY;
	private static float mapMinY;
	private static List<string> destinations = new List<string>() {"Oslo", "Karlskrona", "Rostock", "Gdynia"};
	private int currentPassables;// How many passable zones the ferry is in

	public Ferry(){
		
	}

	void Start(){
		startPoint = gameObject.transform.position;
		map = GameObject.Find ("Map");
		mapMinX = map.transform.position.x;
		mapMaxX = (map.transform.position.x + map.GetComponent<SpriteRenderer>().bounds.size.x);
		mapMaxY = map.transform.position.y;
		mapMinY = (map.transform.position.y - map.GetComponent<SpriteRenderer>().bounds.size.y);
		destination = randomDestination ();
		assignFlag ();
		dragging = false;
		resetPassable ();
		//Set the size according to screen width
		double width = Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height;
		float size = (float)width / 11.0f;
		transform.localScale = new Vector3(size, size, size);
	}

	void Update() {
		if (dragging) {
			drag ();
		}
	}



	public void setColliderEnabled(bool enabled){
		CircleCollider2D cc = gameObject.GetComponent<CircleCollider2D>();
		cc.enabled = enabled;
	}

	public void startDrag(){
		dragging = true;
	}

	public void stopDrag(){
		dragging = false;
	}

	public void goBack(){
		gameObject.transform.position = startPoint;
		stopDrag ();
		resetPassable ();
	}

	public bool checkDestination(string name){
		if (destination == name) {
			return true;
		} else {
			goBack ();
			return false;
		}
	}

	public void addPassable(){
		currentPassables++;
	}

	public void removePassable(){
		currentPassables--;
	}
		
	/// <summary>
	/// Checks if we are in no passable zones
	/// </summary>
	/// <returns>true if we aren't in any passable</returns>
	public bool noPassable(){
		if (currentPassables <= 0) {
			return true;
		} else {
			return false;
		}
	}

	private void resetPassable(){
		currentPassables = 1; //We start in a passable zone
	}

	private void drag(){
		Vector3 mousePos = Input.mousePosition;
		Vector3 worldMousePos = Camera.main.ScreenToWorldPoint (mousePos);
		if (worldMousePos.x > mapMinX && worldMousePos.x < mapMaxX &&
			worldMousePos.y > mapMinY && worldMousePos.y < mapMaxY) {
			gameObject.transform.position = new Vector3 (worldMousePos.x, worldMousePos.y, -1);
		}
	}
		

	private string randomDestination(){
		int index = Random.Range (0, destinations.Count);
		return destinations [index];
	}

	private void assignFlag(){
		GameObject flagObj = Resources.Load ("Minigame_Routes/Flags/" + destination) as GameObject;
		Sprite sprite = flagObj.GetComponent<SpriteRenderer> ().sprite;
		gameObject.transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = sprite;
	}


}
