﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	//------------------UI---------------
	public GameObject failPopupImage;
	public Animator successPopup;
	public Animator failPopup;
	public Animator wrongDestPopup;
	public Text timer;

	//---------------Game World objects-----------
	public GameObject map;
	public List<GameObject> ferriesInGame;
	public GameObject ferryPrefab; //The prefab to be spawned
	public GameObject ferrySpawn; //Spawn point of ferries

	private Ferry ferryDragged;
	private Collider2D ferryCollider;

	//-------------Sound----------
	public AudioClip successSound;
	public AudioClip failSound;
	private AudioSource source;

	//-----------Game state booleans--------
	public bool gameOver = false;
	public bool dragging = false; //Are we dragging a ferry

	private bool correctDestination;


	//---------------Variables---------
	public int score = 0;

	private int ferriesOnQueue = 5;
	private float gameTimer; //Decreases each frame
	private float spawnTimer;
	private int optimalFinishTime;//Highest possible number for gameTimer on finish
	private string destinationEntered;


	//--------------Constants-------------
	private const int MAX_FERRIES = 3;
	private const float SPAWN_DELAY = 4;
	private const int MAX_SCORE = 1000;



	void Start(){
		source = gameObject.GetComponent<AudioSource> ();
		gameTimer = 180;
		optimalFinishTime = Mathf.RoundToInt (gameTimer - (2 * ferriesOnQueue)); //Taking max 2 seconds per ferry.
		spawnTimer = SPAWN_DELAY - 1; //1 second to spawn
	}

	void Update(){
		if (!gameOver) {
			gameTimer -= Time.deltaTime;
			if (gameTimer < 0.4) {
				endGame (false);
				return;
			}
			updateTimer ();
			spawnTimer += Time.deltaTime;
			if (ferriesInGame.Count < MAX_FERRIES && spawnTimer >= SPAWN_DELAY) {
				spawnFerry ();
				spawnTimer = 0;
			}
		}
	}

	/// <summary>
	/// Starts dragging the ferry
	/// </summary>
	/// <param name="f">Ferry selected</param>
	public void setFerryDragged (Ferry f){
		ferryDragged = f;
		ferryCollider = f.gameObject.GetComponent<CircleCollider2D> ();
		ferryDragged.startDrag ();
		dragging = true;
	}

	/// <summary>
	/// Stops dragging the ferry (if any)
	/// If the ferry is in a destination, checks if it's the correct one,
	/// and handles the consequences of that
	/// </summary>
	public void stopDrag(){
		if (dragging) { //Are we dragging a ferry
			if (destinationEntered != null) {
				correctDestination = ferryDragged.checkDestination (destinationEntered); // see if we are in correct place
				if (correctDestination) {
					ferryArrived (ferryDragged.gameObject);
				} else {
					wrongDestPopup.SetTrigger ("fail");
					playFailSound ();
				}
			}
			ferryDragged.stopDrag ();
			ferryDragged = null;
			dragging = false;
		}
	}
		
	public void enterDestination(string destination){
		destinationEntered = destination;
	}

	public void exitDestination(){
		destinationEntered = null;
	}

	/// <summary>
	/// Sets wether on not the ferry is in a passable zone
	/// Called by the passables trigger events
	/// </summary>
	/// <param name="isPassing">If we are in a passable zone</param>
	/// <param name="collided">The object that triggered the event</param>
	public void setPassable (bool isPassing, Collider2D collided){
		if (ferryDragged == null) {
			return; 
		}
		//Only change value if it was triggered by the selected ferry (the one we're dragging)
		if (collided == ferryCollider) {
			if (isPassing == true) {
				ferryDragged.addPassable ();
			} else {
				ferryDragged.removePassable ();
				if (ferryDragged.noPassable()) {
				}
			}

		}
	}

	/// <summary>
	/// Handles the events of a ferry succesfully arriving
	/// (sound, remove this ferry, spawn a new ferry, etc..)
	/// </summary>
	/// <param name="ferry">The ferry that has arrived</param>
	public void ferryArrived (GameObject ferry){
		successPopup.SetTrigger ("fail");//Same trigger, same animation, for now
		source.PlayOneShot(successSound, 0.8f);
		destinationEntered = null;
		ferriesInGame.Remove (ferry);
		Destroy (ferry);
		if (ferriesOnQueue > 0) {
			spawnFerry ();
		}
		if (ferriesInGame.Count <= 0) { //All ferries have been delivered!
			endGame (true);
			return;
		}
	}

	/// <summary>
	/// Handles the events of hitting ground
	/// </summary>
	public void hitGround(Collider2D other){
		if (ferryDragged != null) {
			//If triggered by dragged collider, and not in any passable triggers
			if (ferryDragged.noPassable() && other == ferryCollider) {
				playFailSound ();
				failPopup.SetTrigger ("fail"); 
				ferryDragged.goBack (); //Snap to original position
				dragging = false;
				ferryDragged = null;
			}
		}
	}


	public void playFailSound(){
		if (!source.isPlaying && dragging) {
			source.PlayOneShot (failSound, 0.5f);
		}
	}
		

	/// <summary>
	/// Spawns a ferry, if the max number is not reached, and if there are more ferries left in "queue"
	/// </summary>
	private void spawnFerry(){
		if (ferriesOnQueue <= 0) { //No ferries left
			return;
		}
		GameObject newFerry = (GameObject)Instantiate (ferryPrefab);
		randomizePosition (newFerry);

		ferriesInGame.Add (newFerry);
		ferriesOnQueue--;

	}

	/// <summary>
	/// Randomizes the position of the ferry, within the boundaries of the ferry spawn point
	/// </summary>
	private void randomizePosition (GameObject newFerry){
		Bounds bounds = ferrySpawn.GetComponent<BoxCollider2D>().bounds;
		float randomX = Random.Range (bounds.min.x, bounds.max.x);
		float randomY = Random.Range (bounds.min.y, bounds.max.y);
		newFerry.transform.position = new Vector3(randomX, randomY, ferrySpawn.transform.position.z);
	}

	/// <summary>
	/// Updates the UI timer with correctly formatted time data
	/// </summary>
	private void updateTimer(){
		float minutes = Mathf.FloorToInt(gameTimer / 60);
		float seconds = Mathf.FloorToInt (gameTimer % 60);
		string secondsString;
		if (seconds == 60) {
			secondsString = "00";
		} else if (seconds < 10) {
			secondsString = "0" + seconds.ToString ();
		} else {
			secondsString = seconds.ToString ();
		}
		timer.text = minutes.ToString() + ":" + secondsString;
	}

	/// <summary>
	/// Ends the game and calculates score.
	/// </summary>
	/// <param name="win">Did we successfully deliver all ferries?</param>
	private void endGame(bool win){
		gameOver = true;
		int difference = optimalFinishTime - Mathf.RoundToInt (gameTimer); //How far from optimal time
		score = MAX_SCORE - (difference * (MAX_SCORE/100));//each second detracts 1%
		if (score < 0) {
			score = 0;
		}
		print ("SCORE : " + score.ToString());
	}
}
