﻿using UnityEngine;
using System.Collections;

public class InputScript : MonoBehaviour {
	private Transform hitObj;
	private Ferry ferry;
	private GameManager gamemanager;

	void Start(){
		gamemanager = gameObject.GetComponent<GameManager> ();
	}

	void Update() {
		if (!gamemanager.gameOver) {
			if (Input.GetMouseButtonDown (0)) {//press
				castRay ();
			}

			if (Input.GetMouseButtonUp (0)) {//release
				if (ferry != null) {
					stopDrag ();
				}
			}

			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Ended) {//release
					stopDrag ();
				} else if (touch.phase == TouchPhase.Began) { //press
					castRay ();
				}
			}

		} else { //Game is over, so disable input
			if (ferry != null) {
				ferry.dragging = false; 
				ferry = null;
			}
		}

	}


	private void stopDrag(){
		gamemanager.stopDrag ();
	}

	/// <summary>
	/// Casts a ray, if we hit a ferry object, start dragging it
	/// </summary>
	private void castRay(){
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		if (hit) {
			hitObj = hit.transform;
			if (hitObj.tag == "Ferry") {
				ferry = hitObj.GetComponent<Ferry> ();
				gamemanager.setFerryDragged (ferry);
			}
		} 
	}
}
