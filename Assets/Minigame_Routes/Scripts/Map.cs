﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour {

	private GameManager gamemanager;
	// Use this for initialization
	void Start () {
		gamemanager = GameObject.Find ("GameManager").GetComponent<GameManager> ();

		//----Width
		double width = Camera.main.orthographicSize * 2.0 * Screen.width/ Screen.height;
		Sprite s = gameObject.GetComponent<SpriteRenderer>().sprite;
		float unitWidth = s.textureRect.width / s.pixelsPerUnit;
		float unitHeight = s.textureRect.height / s.pixelsPerUnit;

		//----Position----------
		float dist = (transform.position - Camera.main.transform.position).z;
		float leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;

		gameObject.transform.position = new Vector3 (leftBorder, unitHeight/2);

		//--------------Ratio
		float ratio = unitWidth/unitHeight;

		//-------Height
		float height = Camera.main.orthographicSize * 2;

		//---------Size-----------
		gameObject.transform.localScale = new Vector3((float)(width / unitWidth), (height*ratio/unitHeight));

		//-----Reposition after scaling
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x, height/2 - 
			(gameObject.transform.localScale.y * 0.8f));
	}
	
	// Update is called once per frame
	void Update () {

	}


	void OnTriggerStay2D(Collider2D other) {
		gamemanager.hitGround (other);
	}
}
