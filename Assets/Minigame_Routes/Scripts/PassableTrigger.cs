﻿using UnityEngine;
using System.Collections;

public class PassableTrigger : MonoBehaviour {
	public static GameManager gamemanager;
	// Use this for initialization
	void Start () {
		gamemanager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		gamemanager.setPassable (true, other);

	}

	void OnTriggerExit2D(Collider2D other) {
		gamemanager.setPassable (false, other);
	}
}
