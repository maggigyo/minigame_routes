﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// Ferry
struct Ferry_t67768570;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2369589051  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject GameManager::failPopupImage
	GameObject_t3674682005 * ___failPopupImage_5;
	// UnityEngine.Animator GameManager::successPopup
	Animator_t2776330603 * ___successPopup_6;
	// UnityEngine.Animator GameManager::failPopup
	Animator_t2776330603 * ___failPopup_7;
	// UnityEngine.Animator GameManager::wrongDestPopup
	Animator_t2776330603 * ___wrongDestPopup_8;
	// UnityEngine.UI.Text GameManager::timer
	Text_t9039225 * ___timer_9;
	// UnityEngine.GameObject GameManager::map
	GameObject_t3674682005 * ___map_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::ferriesInGame
	List_1_t747900261 * ___ferriesInGame_11;
	// UnityEngine.GameObject GameManager::ferryPrefab
	GameObject_t3674682005 * ___ferryPrefab_12;
	// UnityEngine.GameObject GameManager::ferrySpawn
	GameObject_t3674682005 * ___ferrySpawn_13;
	// Ferry GameManager::ferryDragged
	Ferry_t67768570 * ___ferryDragged_14;
	// UnityEngine.Collider2D GameManager::ferryCollider
	Collider2D_t1552025098 * ___ferryCollider_15;
	// UnityEngine.AudioClip GameManager::successSound
	AudioClip_t794140988 * ___successSound_16;
	// UnityEngine.AudioClip GameManager::failSound
	AudioClip_t794140988 * ___failSound_17;
	// UnityEngine.AudioSource GameManager::source
	AudioSource_t1740077639 * ___source_18;
	// System.Boolean GameManager::gameOver
	bool ___gameOver_19;
	// System.Boolean GameManager::dragging
	bool ___dragging_20;
	// System.Boolean GameManager::correctDestination
	bool ___correctDestination_21;
	// System.Int32 GameManager::score
	int32_t ___score_22;
	// System.Int32 GameManager::ferriesOnQueue
	int32_t ___ferriesOnQueue_23;
	// System.Single GameManager::gameTimer
	float ___gameTimer_24;
	// System.Single GameManager::spawnTimer
	float ___spawnTimer_25;
	// System.Int32 GameManager::optimalFinishTime
	int32_t ___optimalFinishTime_26;
	// System.String GameManager::destinationEntered
	String_t* ___destinationEntered_27;

public:
	inline static int32_t get_offset_of_failPopupImage_5() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___failPopupImage_5)); }
	inline GameObject_t3674682005 * get_failPopupImage_5() const { return ___failPopupImage_5; }
	inline GameObject_t3674682005 ** get_address_of_failPopupImage_5() { return &___failPopupImage_5; }
	inline void set_failPopupImage_5(GameObject_t3674682005 * value)
	{
		___failPopupImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___failPopupImage_5, value);
	}

	inline static int32_t get_offset_of_successPopup_6() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___successPopup_6)); }
	inline Animator_t2776330603 * get_successPopup_6() const { return ___successPopup_6; }
	inline Animator_t2776330603 ** get_address_of_successPopup_6() { return &___successPopup_6; }
	inline void set_successPopup_6(Animator_t2776330603 * value)
	{
		___successPopup_6 = value;
		Il2CppCodeGenWriteBarrier(&___successPopup_6, value);
	}

	inline static int32_t get_offset_of_failPopup_7() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___failPopup_7)); }
	inline Animator_t2776330603 * get_failPopup_7() const { return ___failPopup_7; }
	inline Animator_t2776330603 ** get_address_of_failPopup_7() { return &___failPopup_7; }
	inline void set_failPopup_7(Animator_t2776330603 * value)
	{
		___failPopup_7 = value;
		Il2CppCodeGenWriteBarrier(&___failPopup_7, value);
	}

	inline static int32_t get_offset_of_wrongDestPopup_8() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___wrongDestPopup_8)); }
	inline Animator_t2776330603 * get_wrongDestPopup_8() const { return ___wrongDestPopup_8; }
	inline Animator_t2776330603 ** get_address_of_wrongDestPopup_8() { return &___wrongDestPopup_8; }
	inline void set_wrongDestPopup_8(Animator_t2776330603 * value)
	{
		___wrongDestPopup_8 = value;
		Il2CppCodeGenWriteBarrier(&___wrongDestPopup_8, value);
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___timer_9)); }
	inline Text_t9039225 * get_timer_9() const { return ___timer_9; }
	inline Text_t9039225 ** get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(Text_t9039225 * value)
	{
		___timer_9 = value;
		Il2CppCodeGenWriteBarrier(&___timer_9, value);
	}

	inline static int32_t get_offset_of_map_10() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___map_10)); }
	inline GameObject_t3674682005 * get_map_10() const { return ___map_10; }
	inline GameObject_t3674682005 ** get_address_of_map_10() { return &___map_10; }
	inline void set_map_10(GameObject_t3674682005 * value)
	{
		___map_10 = value;
		Il2CppCodeGenWriteBarrier(&___map_10, value);
	}

	inline static int32_t get_offset_of_ferriesInGame_11() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferriesInGame_11)); }
	inline List_1_t747900261 * get_ferriesInGame_11() const { return ___ferriesInGame_11; }
	inline List_1_t747900261 ** get_address_of_ferriesInGame_11() { return &___ferriesInGame_11; }
	inline void set_ferriesInGame_11(List_1_t747900261 * value)
	{
		___ferriesInGame_11 = value;
		Il2CppCodeGenWriteBarrier(&___ferriesInGame_11, value);
	}

	inline static int32_t get_offset_of_ferryPrefab_12() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferryPrefab_12)); }
	inline GameObject_t3674682005 * get_ferryPrefab_12() const { return ___ferryPrefab_12; }
	inline GameObject_t3674682005 ** get_address_of_ferryPrefab_12() { return &___ferryPrefab_12; }
	inline void set_ferryPrefab_12(GameObject_t3674682005 * value)
	{
		___ferryPrefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___ferryPrefab_12, value);
	}

	inline static int32_t get_offset_of_ferrySpawn_13() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferrySpawn_13)); }
	inline GameObject_t3674682005 * get_ferrySpawn_13() const { return ___ferrySpawn_13; }
	inline GameObject_t3674682005 ** get_address_of_ferrySpawn_13() { return &___ferrySpawn_13; }
	inline void set_ferrySpawn_13(GameObject_t3674682005 * value)
	{
		___ferrySpawn_13 = value;
		Il2CppCodeGenWriteBarrier(&___ferrySpawn_13, value);
	}

	inline static int32_t get_offset_of_ferryDragged_14() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferryDragged_14)); }
	inline Ferry_t67768570 * get_ferryDragged_14() const { return ___ferryDragged_14; }
	inline Ferry_t67768570 ** get_address_of_ferryDragged_14() { return &___ferryDragged_14; }
	inline void set_ferryDragged_14(Ferry_t67768570 * value)
	{
		___ferryDragged_14 = value;
		Il2CppCodeGenWriteBarrier(&___ferryDragged_14, value);
	}

	inline static int32_t get_offset_of_ferryCollider_15() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferryCollider_15)); }
	inline Collider2D_t1552025098 * get_ferryCollider_15() const { return ___ferryCollider_15; }
	inline Collider2D_t1552025098 ** get_address_of_ferryCollider_15() { return &___ferryCollider_15; }
	inline void set_ferryCollider_15(Collider2D_t1552025098 * value)
	{
		___ferryCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___ferryCollider_15, value);
	}

	inline static int32_t get_offset_of_successSound_16() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___successSound_16)); }
	inline AudioClip_t794140988 * get_successSound_16() const { return ___successSound_16; }
	inline AudioClip_t794140988 ** get_address_of_successSound_16() { return &___successSound_16; }
	inline void set_successSound_16(AudioClip_t794140988 * value)
	{
		___successSound_16 = value;
		Il2CppCodeGenWriteBarrier(&___successSound_16, value);
	}

	inline static int32_t get_offset_of_failSound_17() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___failSound_17)); }
	inline AudioClip_t794140988 * get_failSound_17() const { return ___failSound_17; }
	inline AudioClip_t794140988 ** get_address_of_failSound_17() { return &___failSound_17; }
	inline void set_failSound_17(AudioClip_t794140988 * value)
	{
		___failSound_17 = value;
		Il2CppCodeGenWriteBarrier(&___failSound_17, value);
	}

	inline static int32_t get_offset_of_source_18() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___source_18)); }
	inline AudioSource_t1740077639 * get_source_18() const { return ___source_18; }
	inline AudioSource_t1740077639 ** get_address_of_source_18() { return &___source_18; }
	inline void set_source_18(AudioSource_t1740077639 * value)
	{
		___source_18 = value;
		Il2CppCodeGenWriteBarrier(&___source_18, value);
	}

	inline static int32_t get_offset_of_gameOver_19() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___gameOver_19)); }
	inline bool get_gameOver_19() const { return ___gameOver_19; }
	inline bool* get_address_of_gameOver_19() { return &___gameOver_19; }
	inline void set_gameOver_19(bool value)
	{
		___gameOver_19 = value;
	}

	inline static int32_t get_offset_of_dragging_20() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___dragging_20)); }
	inline bool get_dragging_20() const { return ___dragging_20; }
	inline bool* get_address_of_dragging_20() { return &___dragging_20; }
	inline void set_dragging_20(bool value)
	{
		___dragging_20 = value;
	}

	inline static int32_t get_offset_of_correctDestination_21() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___correctDestination_21)); }
	inline bool get_correctDestination_21() const { return ___correctDestination_21; }
	inline bool* get_address_of_correctDestination_21() { return &___correctDestination_21; }
	inline void set_correctDestination_21(bool value)
	{
		___correctDestination_21 = value;
	}

	inline static int32_t get_offset_of_score_22() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___score_22)); }
	inline int32_t get_score_22() const { return ___score_22; }
	inline int32_t* get_address_of_score_22() { return &___score_22; }
	inline void set_score_22(int32_t value)
	{
		___score_22 = value;
	}

	inline static int32_t get_offset_of_ferriesOnQueue_23() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___ferriesOnQueue_23)); }
	inline int32_t get_ferriesOnQueue_23() const { return ___ferriesOnQueue_23; }
	inline int32_t* get_address_of_ferriesOnQueue_23() { return &___ferriesOnQueue_23; }
	inline void set_ferriesOnQueue_23(int32_t value)
	{
		___ferriesOnQueue_23 = value;
	}

	inline static int32_t get_offset_of_gameTimer_24() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___gameTimer_24)); }
	inline float get_gameTimer_24() const { return ___gameTimer_24; }
	inline float* get_address_of_gameTimer_24() { return &___gameTimer_24; }
	inline void set_gameTimer_24(float value)
	{
		___gameTimer_24 = value;
	}

	inline static int32_t get_offset_of_spawnTimer_25() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___spawnTimer_25)); }
	inline float get_spawnTimer_25() const { return ___spawnTimer_25; }
	inline float* get_address_of_spawnTimer_25() { return &___spawnTimer_25; }
	inline void set_spawnTimer_25(float value)
	{
		___spawnTimer_25 = value;
	}

	inline static int32_t get_offset_of_optimalFinishTime_26() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___optimalFinishTime_26)); }
	inline int32_t get_optimalFinishTime_26() const { return ___optimalFinishTime_26; }
	inline int32_t* get_address_of_optimalFinishTime_26() { return &___optimalFinishTime_26; }
	inline void set_optimalFinishTime_26(int32_t value)
	{
		___optimalFinishTime_26 = value;
	}

	inline static int32_t get_offset_of_destinationEntered_27() { return static_cast<int32_t>(offsetof(GameManager_t2369589051, ___destinationEntered_27)); }
	inline String_t* get_destinationEntered_27() const { return ___destinationEntered_27; }
	inline String_t** get_address_of_destinationEntered_27() { return &___destinationEntered_27; }
	inline void set_destinationEntered_27(String_t* value)
	{
		___destinationEntered_27 = value;
		Il2CppCodeGenWriteBarrier(&___destinationEntered_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
