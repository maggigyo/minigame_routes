﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager
struct GameManager_t2369589051;
// Ferry
struct Ferry_t67768570;
// System.String
struct String_t;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ferry67768570.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m4112277136 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Start()
extern "C"  void GameManager_Start_m3059414928 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Update()
extern "C"  void GameManager_Update_m358434429 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::setFerryDragged(Ferry)
extern "C"  void GameManager_setFerryDragged_m3525614672 (GameManager_t2369589051 * __this, Ferry_t67768570 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::stopDrag()
extern "C"  void GameManager_stopDrag_m1676044778 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::enterDestination(System.String)
extern "C"  void GameManager_enterDestination_m3079206552 (GameManager_t2369589051 * __this, String_t* ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::exitDestination()
extern "C"  void GameManager_exitDestination_m1575616414 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::setPassable(System.Boolean,UnityEngine.Collider2D)
extern "C"  void GameManager_setPassable_m2555515383 (GameManager_t2369589051 * __this, bool ___isPassing0, Collider2D_t1552025098 * ___collided1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::ferryArrived(UnityEngine.GameObject)
extern "C"  void GameManager_ferryArrived_m3307230975 (GameManager_t2369589051 * __this, GameObject_t3674682005 * ___ferry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::hitGround(UnityEngine.Collider2D)
extern "C"  void GameManager_hitGround_m4148382315 (GameManager_t2369589051 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::playFailSound()
extern "C"  void GameManager_playFailSound_m4039821131 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::spawnFerry()
extern "C"  void GameManager_spawnFerry_m1563167987 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::randomizePosition(UnityEngine.GameObject)
extern "C"  void GameManager_randomizePosition_m2150844736 (GameManager_t2369589051 * __this, GameObject_t3674682005 * ___newFerry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::updateTimer()
extern "C"  void GameManager_updateTimer_m1621426954 (GameManager_t2369589051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::endGame(System.Boolean)
extern "C"  void GameManager_endGame_m2581740274 (GameManager_t2369589051 * __this, bool ___win0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
