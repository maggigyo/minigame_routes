﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputScript
struct InputScript_t1346190613;

#include "codegen/il2cpp-codegen.h"

// System.Void InputScript::.ctor()
extern "C"  void InputScript__ctor_m2173249014 (InputScript_t1346190613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputScript::Start()
extern "C"  void InputScript_Start_m1120386806 (InputScript_t1346190613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputScript::Update()
extern "C"  void InputScript_Update_m378104791 (InputScript_t1346190613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputScript::stopDrag()
extern "C"  void InputScript_stopDrag_m3399393476 (InputScript_t1346190613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputScript::castRay()
extern "C"  void InputScript_castRay_m4164570143 (InputScript_t1346190613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
