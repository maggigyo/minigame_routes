﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PassableTrigger
struct PassableTrigger_t3737800237;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void PassableTrigger::.ctor()
extern "C"  void PassableTrigger__ctor_m1207835614 (PassableTrigger_t3737800237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PassableTrigger::Start()
extern "C"  void PassableTrigger_Start_m154973406 (PassableTrigger_t3737800237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PassableTrigger::Update()
extern "C"  void PassableTrigger_Update_m515060463 (PassableTrigger_t3737800237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PassableTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PassableTrigger_OnTriggerEnter2D_m151156570 (PassableTrigger_t3737800237 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PassableTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PassableTrigger_OnTriggerExit2D_m4249680392 (PassableTrigger_t3737800237 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
