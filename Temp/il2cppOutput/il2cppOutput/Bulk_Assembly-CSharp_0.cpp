﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Destination
struct Destination_t238021614;
// GameManager
struct GameManager_t2369589051;
// System.Object
struct Il2CppObject;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// Ferry
struct Ferry_t67768570;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2548470764;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t3380110778;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t2212926951;
// InputScript
struct InputScript_t1346190613;
// Map
struct Map_t77116;
// PassableTrigger
struct PassableTrigger_t3737800237;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Destination238021614.h"
#include "AssemblyU2DCSharp_Destination238021614MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_GameManager2369589051.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "AssemblyU2DCSharp_GameManager2369589051MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ferry67768570.h"
#include "AssemblyU2DCSharp_Ferry67768570MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CircleCollider2D3380110778.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen747900261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen747900261.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D2212926951.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_InputScript1346190613.h"
#include "AssemblyU2DCSharp_InputScript1346190613MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D9846735MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Map77116.h"
#include "AssemblyU2DCSharp_Map77116MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite3199167241MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_PassableTrigger3737800237.h"
#include "AssemblyU2DCSharp_PassableTrigger3737800237MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GameManager>()
#define GameObject_GetComponent_TisGameManager_t2369589051_m494774470(__this, method) ((  GameManager_t2369589051 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(__this, method) ((  SpriteRenderer_t2548470764 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CircleCollider2D>()
#define GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224(__this, method) ((  CircleCollider2D_t3380110778 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t2548470764_m4090179846(__this, method) ((  SpriteRenderer_t2548470764 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(__this, method) ((  AudioSource_t1740077639 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t3674682005_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t2212926951_m262378119(__this, method) ((  BoxCollider2D_t2212926951 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Ferry>()
#define Component_GetComponent_TisFerry_t67768570_m132757375(__this, method) ((  Ferry_t67768570 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Destination::.ctor()
extern "C"  void Destination__ctor_m1358633789 (Destination_t238021614 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Destination::Start()
extern Il2CppClass* Destination_t238021614_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2369589051;
extern const uint32_t Destination_Start_m305771581_MetadataUsageId;
extern "C"  void Destination_Start_m305771581 (Destination_t238021614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Destination_Start_m305771581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2369589051, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameManager_t2369589051 * L_1 = GameObject_GetComponent_TisGameManager_t2369589051_m494774470(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var);
		((Destination_t238021614_StaticFields*)Destination_t238021614_il2cpp_TypeInfo_var->static_fields)->set_gamemanager_4(L_1);
		return;
	}
}
// System.Void Destination::Update()
extern "C"  void Destination_Update_m894836592 (Destination_t238021614 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Destination::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* Destination_t238021614_il2cpp_TypeInfo_var;
extern const uint32_t Destination_OnTriggerEnter2D_m1655381979_MetadataUsageId;
extern "C"  void Destination_OnTriggerEnter2D_m1655381979 (Destination_t238021614 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Destination_OnTriggerEnter2D_m1655381979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameManager_t2369589051 * L_0 = ((Destination_t238021614_StaticFields*)Destination_t238021614_il2cpp_TypeInfo_var->static_fields)->get_gamemanager_4();
		String_t* L_1 = __this->get_name_2();
		NullCheck(L_0);
		GameManager_enterDestination_m3079206552(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Destination::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* Destination_t238021614_il2cpp_TypeInfo_var;
extern const uint32_t Destination_OnTriggerExit2D_m2912730471_MetadataUsageId;
extern "C"  void Destination_OnTriggerExit2D_m2912730471 (Destination_t238021614 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Destination_OnTriggerExit2D_m2912730471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameManager_t2369589051 * L_0 = ((Destination_t238021614_StaticFields*)Destination_t238021614_il2cpp_TypeInfo_var->static_fields)->get_gamemanager_4();
		NullCheck(L_0);
		GameManager_exitDestination_m1575616414(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ferry::.ctor()
extern "C"  void Ferry__ctor_m905327025 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ferry::.cctor()
extern Il2CppClass* List_1_t1375417109_il2cpp_TypeInfo_var;
extern Il2CppClass* Ferry_t67768570_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4975193_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2467463;
extern Il2CppCodeGenString* _stringLiteral489673016;
extern Il2CppCodeGenString* _stringLiteral3048464089;
extern Il2CppCodeGenString* _stringLiteral2128735594;
extern const uint32_t Ferry__cctor_m1813237788_MetadataUsageId;
extern "C"  void Ferry__cctor_m1813237788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry__cctor_m1813237788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1375417109 * V_0 = NULL;
	{
		List_1_t1375417109 * L_0 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		V_0 = L_0;
		List_1_t1375417109 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m4975193(L_1, _stringLiteral2467463, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_2 = V_0;
		NullCheck(L_2);
		List_1_Add_m4975193(L_2, _stringLiteral489673016, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Add_m4975193(L_3, _stringLiteral3048464089, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_4 = V_0;
		NullCheck(L_4);
		List_1_Add_m4975193(L_4, _stringLiteral2128735594, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_5 = V_0;
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_destinations_10(L_5);
		return;
	}
}
// System.Void Ferry::Start()
extern Il2CppClass* Ferry_t67768570_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral77116;
extern const uint32_t Ferry_Start_m4147432113_MetadataUsageId;
extern "C"  void Ferry_Start_m4147432113 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_Start_m4147432113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	float V_1 = 0.0f;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t2711641849  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Bounds_t2711641849  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set_startPoint_5(L_2);
		GameObject_t3674682005 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral77116, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_map_3(L_3);
		GameObject_t3674682005 * L_4 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_x_1();
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_mapMinX_7(L_7);
		GameObject_t3674682005 * L_8 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_x_1();
		GameObject_t3674682005 * L_12 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_12);
		SpriteRenderer_t2548470764 * L_13 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_12, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_13);
		Bounds_t2711641849  L_14 = Renderer_get_bounds_m1533373851(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		Vector3_t4282066566  L_15 = Bounds_get_size_m3666348432((&V_4), /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_x_1();
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_mapMaxX_6(((float)((float)L_11+(float)L_16)));
		GameObject_t3674682005 * L_17 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = (&V_6)->get_y_2();
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_mapMaxY_8(L_20);
		GameObject_t3674682005 * L_21 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		float L_24 = (&V_7)->get_y_2();
		GameObject_t3674682005 * L_25 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_map_3();
		NullCheck(L_25);
		SpriteRenderer_t2548470764 * L_26 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_25, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_26);
		Bounds_t2711641849  L_27 = Renderer_get_bounds_m1533373851(L_26, /*hidden argument*/NULL);
		V_8 = L_27;
		Vector3_t4282066566  L_28 = Bounds_get_size_m3666348432((&V_8), /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = (&V_9)->get_y_2();
		((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->set_mapMinY_9(((float)((float)L_24-(float)L_29)));
		String_t* L_30 = Ferry_randomDestination_m4104054493(__this, /*hidden argument*/NULL);
		__this->set_destination_4(L_30);
		Ferry_assignFlag_m2147000846(__this, /*hidden argument*/NULL);
		__this->set_dragging_2((bool)0);
		Ferry_resetPassable_m3221425609(__this, /*hidden argument*/NULL);
		Camera_t2727095145 * L_31 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		float L_32 = Camera_get_orthographicSize_m3215515490(L_31, /*hidden argument*/NULL);
		int32_t L_33 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_34 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((double)((double)((double)((double)((double)((double)(((double)((double)L_32)))*(double)(2.0)))*(double)(((double)((double)L_33)))))/(double)(((double)((double)L_34)))));
		double L_35 = V_0;
		V_1 = ((float)((float)(((float)((float)L_35)))/(float)(11.0f)));
		Transform_t1659122786 * L_36 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_37 = V_1;
		float L_38 = V_1;
		float L_39 = V_1;
		Vector3_t4282066566  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m2926210380(&L_40, L_37, L_38, L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_localScale_m310756934(L_36, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ferry::Update()
extern "C"  void Ferry_Update_m4022196092 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_dragging_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Ferry_drag_m1311360391(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void Ferry::setColliderEnabled(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224_MethodInfo_var;
extern const uint32_t Ferry_setColliderEnabled_m1897876469_MetadataUsageId;
extern "C"  void Ferry_setColliderEnabled_m1897876469 (Ferry_t67768570 * __this, bool ___enabled0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_setColliderEnabled_m1897876469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CircleCollider2D_t3380110778 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		CircleCollider2D_t3380110778 * L_1 = GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224(L_0, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224_MethodInfo_var);
		V_0 = L_1;
		CircleCollider2D_t3380110778 * L_2 = V_0;
		bool L_3 = ___enabled0;
		NullCheck(L_2);
		Behaviour_set_enabled_m2046806933(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ferry::startDrag()
extern "C"  void Ferry_startDrag_m906913573 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		__this->set_dragging_2((bool)1);
		return;
	}
}
// System.Void Ferry::stopDrag()
extern "C"  void Ferry_stopDrag_m677820201 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		__this->set_dragging_2((bool)0);
		return;
	}
}
// System.Void Ferry::goBack()
extern "C"  void Ferry_goBack_m3466615554 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = __this->get_startPoint_5();
		NullCheck(L_1);
		Transform_set_position_m3111394108(L_1, L_2, /*hidden argument*/NULL);
		Ferry_stopDrag_m677820201(__this, /*hidden argument*/NULL);
		Ferry_resetPassable_m3221425609(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Ferry::checkDestination(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Ferry_checkDestination_m2217443485_MetadataUsageId;
extern "C"  bool Ferry_checkDestination_m2217443485 (Ferry_t67768570 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_checkDestination_m2217443485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_destination_4();
		String_t* L_1 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		Ferry_goBack_m3466615554(__this, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void Ferry::addPassable()
extern "C"  void Ferry_addPassable_m1051725339 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentPassables_11();
		__this->set_currentPassables_11(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Ferry::removePassable()
extern "C"  void Ferry_removePassable_m2628931746 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentPassables_11();
		__this->set_currentPassables_11(((int32_t)((int32_t)L_0-(int32_t)1)));
		return;
	}
}
// System.Boolean Ferry::noPassable()
extern "C"  bool Ferry_noPassable_m3164308331 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentPassables_11();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		return (bool)0;
	}
}
// System.Void Ferry::resetPassable()
extern "C"  void Ferry_resetPassable_m3221425609 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	{
		__this->set_currentPassables_11(1);
		return;
	}
}
// System.Void Ferry::drag()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Ferry_t67768570_il2cpp_TypeInfo_var;
extern const uint32_t Ferry_drag_m1311360391_MetadataUsageId;
extern "C"  void Ferry_drag_m1311360391 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_drag_m1311360391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t2727095145 * L_1 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = V_0;
		NullCheck(L_1);
		Vector3_t4282066566  L_3 = Camera_ScreenToWorldPoint_m1572306334(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		float L_5 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_mapMinX_7();
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_007e;
		}
	}
	{
		float L_6 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		float L_7 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_mapMaxX_6();
		if ((!(((float)L_6) < ((float)L_7))))
		{
			goto IL_007e;
		}
	}
	{
		float L_8 = (&V_1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		float L_9 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_mapMinY_9();
		if ((!(((float)L_8) > ((float)L_9))))
		{
			goto IL_007e;
		}
	}
	{
		float L_10 = (&V_1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		float L_11 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_mapMaxY_8();
		if ((!(((float)L_10) < ((float)L_11))))
		{
			goto IL_007e;
		}
	}
	{
		GameObject_t3674682005 * L_12 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		float L_14 = (&V_1)->get_x_1();
		float L_15 = (&V_1)->get_y_2();
		Vector3_t4282066566  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2926210380(&L_16, L_14, L_15, (-1.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m3111394108(L_13, L_16, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.String Ferry::randomDestination()
extern Il2CppClass* Ferry_t67768570_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m935595982_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1424439513_MethodInfo_var;
extern const uint32_t Ferry_randomDestination_m4104054493_MetadataUsageId;
extern "C"  String_t* Ferry_randomDestination_m4104054493 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_randomDestination_m4104054493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Ferry_t67768570_il2cpp_TypeInfo_var);
		List_1_t1375417109 * L_0 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_destinations_10();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m935595982(L_0, /*hidden argument*/List_1_get_Count_m935595982_MethodInfo_var);
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t1375417109 * L_3 = ((Ferry_t67768570_StaticFields*)Ferry_t67768570_il2cpp_TypeInfo_var->static_fields)->get_destinations_10();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		String_t* L_5 = List_1_get_Item_m1424439513(L_3, L_4, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
		return L_5;
	}
}
// System.Void Ferry::assignFlag()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t2548470764_m4090179846_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1362475031;
extern const uint32_t Ferry_assignFlag_m2147000846_MetadataUsageId;
extern "C"  void Ferry_assignFlag_m2147000846 (Ferry_t67768570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ferry_assignFlag_m2147000846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Sprite_t3199167241 * V_1 = NULL;
	{
		String_t* L_0 = __this->get_destination_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1362475031, L_0, /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m2187391845(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((GameObject_t3674682005 *)IsInstSealed(L_2, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_3 = V_0;
		NullCheck(L_3);
		SpriteRenderer_t2548470764 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_4);
		Sprite_t3199167241 * L_5 = SpriteRenderer_get_sprite_m3481747968(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Transform_GetChild_m4040462992(L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_8);
		SpriteRenderer_t2548470764 * L_9 = Component_GetComponent_TisSpriteRenderer_t2548470764_m4090179846(L_8, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t2548470764_m4090179846_MethodInfo_var);
		Sprite_t3199167241 * L_10 = V_1;
		NullCheck(L_9);
		SpriteRenderer_set_sprite_m1519408453(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m4112277136 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	{
		__this->set_ferriesOnQueue_23(5);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Start()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t GameManager_Start_m3059414928_MetadataUsageId;
extern "C"  void GameManager_Start_m3059414928 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Start_m3059414928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t1740077639 * L_1 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		__this->set_source_18(L_1);
		__this->set_gameTimer_24((180.0f));
		float L_2 = __this->get_gameTimer_24();
		int32_t L_3 = __this->get_ferriesOnQueue_23();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_2-(float)(((float)((float)((int32_t)((int32_t)2*(int32_t)L_3))))))), /*hidden argument*/NULL);
		__this->set_optimalFinishTime_26(L_4);
		__this->set_spawnTimer_25((3.0f));
		return;
	}
}
// System.Void GameManager::Update()
extern const MethodInfo* List_1_get_Count_m487786338_MethodInfo_var;
extern const uint32_t GameManager_Update_m358434429_MetadataUsageId;
extern "C"  void GameManager_Update_m358434429 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Update_m358434429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_gameOver_19();
		if (L_0)
		{
			goto IL_0084;
		}
	}
	{
		float L_1 = __this->get_gameTimer_24();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_gameTimer_24(((float)((float)L_1-(float)L_2)));
		float L_3 = __this->get_gameTimer_24();
		if ((!(((double)(((double)((double)L_3)))) < ((double)(0.4)))))
		{
			goto IL_003a;
		}
	}
	{
		GameManager_endGame_m2581740274(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		GameManager_updateTimer_m1621426954(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_spawnTimer_25();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_spawnTimer_25(((float)((float)L_4+(float)L_5)));
		List_1_t747900261 * L_6 = __this->get_ferriesInGame_11();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m487786338(L_6, /*hidden argument*/List_1_get_Count_m487786338_MethodInfo_var);
		if ((((int32_t)L_7) >= ((int32_t)3)))
		{
			goto IL_0084;
		}
	}
	{
		float L_8 = __this->get_spawnTimer_25();
		if ((!(((float)L_8) >= ((float)(4.0f)))))
		{
			goto IL_0084;
		}
	}
	{
		GameManager_spawnFerry_m1563167987(__this, /*hidden argument*/NULL);
		__this->set_spawnTimer_25((0.0f));
	}

IL_0084:
	{
		return;
	}
}
// System.Void GameManager::setFerryDragged(Ferry)
extern const MethodInfo* GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224_MethodInfo_var;
extern const uint32_t GameManager_setFerryDragged_m3525614672_MetadataUsageId;
extern "C"  void GameManager_setFerryDragged_m3525614672 (GameManager_t2369589051 * __this, Ferry_t67768570 * ___f0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_setFerryDragged_m3525614672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Ferry_t67768570 * L_0 = ___f0;
		__this->set_ferryDragged_14(L_0);
		Ferry_t67768570 * L_1 = ___f0;
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		CircleCollider2D_t3380110778 * L_3 = GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224(L_2, /*hidden argument*/GameObject_GetComponent_TisCircleCollider2D_t3380110778_m3236006224_MethodInfo_var);
		__this->set_ferryCollider_15(L_3);
		Ferry_t67768570 * L_4 = __this->get_ferryDragged_14();
		NullCheck(L_4);
		Ferry_startDrag_m906913573(L_4, /*hidden argument*/NULL);
		__this->set_dragging_20((bool)1);
		return;
	}
}
// System.Void GameManager::stopDrag()
extern Il2CppCodeGenString* _stringLiteral3135262;
extern const uint32_t GameManager_stopDrag_m1676044778_MetadataUsageId;
extern "C"  void GameManager_stopDrag_m1676044778 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_stopDrag_m1676044778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_dragging_20();
		if (!L_0)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_1 = __this->get_destinationEntered_27();
		if (!L_1)
		{
			goto IL_0064;
		}
	}
	{
		Ferry_t67768570 * L_2 = __this->get_ferryDragged_14();
		String_t* L_3 = __this->get_destinationEntered_27();
		NullCheck(L_2);
		bool L_4 = Ferry_checkDestination_m2217443485(L_2, L_3, /*hidden argument*/NULL);
		__this->set_correctDestination_21(L_4);
		bool L_5 = __this->get_correctDestination_21();
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		Ferry_t67768570 * L_6 = __this->get_ferryDragged_14();
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		GameManager_ferryArrived_m3307230975(__this, L_7, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_004e:
	{
		Animator_t2776330603 * L_8 = __this->get_wrongDestPopup_8();
		NullCheck(L_8);
		Animator_SetTrigger_m514363822(L_8, _stringLiteral3135262, /*hidden argument*/NULL);
		GameManager_playFailSound_m4039821131(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		Ferry_t67768570 * L_9 = __this->get_ferryDragged_14();
		NullCheck(L_9);
		Ferry_stopDrag_m677820201(L_9, /*hidden argument*/NULL);
		__this->set_ferryDragged_14((Ferry_t67768570 *)NULL);
		__this->set_dragging_20((bool)0);
	}

IL_007d:
	{
		return;
	}
}
// System.Void GameManager::enterDestination(System.String)
extern "C"  void GameManager_enterDestination_m3079206552 (GameManager_t2369589051 * __this, String_t* ___destination0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___destination0;
		__this->set_destinationEntered_27(L_0);
		return;
	}
}
// System.Void GameManager::exitDestination()
extern "C"  void GameManager_exitDestination_m1575616414 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	{
		__this->set_destinationEntered_27((String_t*)NULL);
		return;
	}
}
// System.Void GameManager::setPassable(System.Boolean,UnityEngine.Collider2D)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameManager_setPassable_m2555515383_MetadataUsageId;
extern "C"  void GameManager_setPassable_m2555515383 (GameManager_t2369589051 * __this, bool ___isPassing0, Collider2D_t1552025098 * ___collided1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_setPassable_m2555515383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Ferry_t67768570 * L_0 = __this->get_ferryDragged_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Collider2D_t1552025098 * L_2 = ___collided1;
		Collider2D_t1552025098 * L_3 = __this->get_ferryCollider_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0054;
		}
	}
	{
		bool L_5 = ___isPassing0;
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		Ferry_t67768570 * L_6 = __this->get_ferryDragged_14();
		NullCheck(L_6);
		Ferry_addPassable_m1051725339(L_6, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0039:
	{
		Ferry_t67768570 * L_7 = __this->get_ferryDragged_14();
		NullCheck(L_7);
		Ferry_removePassable_m2628931746(L_7, /*hidden argument*/NULL);
		Ferry_t67768570 * L_8 = __this->get_ferryDragged_14();
		NullCheck(L_8);
		bool L_9 = Ferry_noPassable_m3164308331(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0054;
		}
	}

IL_0054:
	{
		return;
	}
}
// System.Void GameManager::ferryArrived(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m1159674165_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m487786338_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3135262;
extern const uint32_t GameManager_ferryArrived_m3307230975_MetadataUsageId;
extern "C"  void GameManager_ferryArrived_m3307230975 (GameManager_t2369589051 * __this, GameObject_t3674682005 * ___ferry0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_ferryArrived_m3307230975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get_successPopup_6();
		NullCheck(L_0);
		Animator_SetTrigger_m514363822(L_0, _stringLiteral3135262, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_1 = __this->get_source_18();
		AudioClip_t794140988 * L_2 = __this->get_successSound_16();
		NullCheck(L_1);
		AudioSource_PlayOneShot_m823779350(L_1, L_2, (0.8f), /*hidden argument*/NULL);
		__this->set_destinationEntered_27((String_t*)NULL);
		List_1_t747900261 * L_3 = __this->get_ferriesInGame_11();
		GameObject_t3674682005 * L_4 = ___ferry0;
		NullCheck(L_3);
		List_1_Remove_m1159674165(L_3, L_4, /*hidden argument*/List_1_Remove_m1159674165_MethodInfo_var);
		GameObject_t3674682005 * L_5 = ___ferry0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_ferriesOnQueue_23();
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		GameManager_spawnFerry_m1563167987(__this, /*hidden argument*/NULL);
	}

IL_0052:
	{
		List_1_t747900261 * L_7 = __this->get_ferriesInGame_11();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m487786338(L_7, /*hidden argument*/List_1_get_Count_m487786338_MethodInfo_var);
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_006b;
		}
	}
	{
		GameManager_endGame_m2581740274(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_006b:
	{
		return;
	}
}
// System.Void GameManager::hitGround(UnityEngine.Collider2D)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3135262;
extern const uint32_t GameManager_hitGround_m4148382315_MetadataUsageId;
extern "C"  void GameManager_hitGround_m4148382315 (GameManager_t2369589051 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_hitGround_m4148382315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Ferry_t67768570 * L_0 = __this->get_ferryDragged_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0061;
		}
	}
	{
		Ferry_t67768570 * L_2 = __this->get_ferryDragged_14();
		NullCheck(L_2);
		bool L_3 = Ferry_noPassable_m3164308331(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0061;
		}
	}
	{
		Collider2D_t1552025098 * L_4 = ___other0;
		Collider2D_t1552025098 * L_5 = __this->get_ferryCollider_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		GameManager_playFailSound_m4039821131(__this, /*hidden argument*/NULL);
		Animator_t2776330603 * L_7 = __this->get_failPopup_7();
		NullCheck(L_7);
		Animator_SetTrigger_m514363822(L_7, _stringLiteral3135262, /*hidden argument*/NULL);
		Ferry_t67768570 * L_8 = __this->get_ferryDragged_14();
		NullCheck(L_8);
		Ferry_goBack_m3466615554(L_8, /*hidden argument*/NULL);
		__this->set_dragging_20((bool)0);
		__this->set_ferryDragged_14((Ferry_t67768570 *)NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void GameManager::playFailSound()
extern "C"  void GameManager_playFailSound_m4039821131 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1740077639 * L_0 = __this->get_source_18();
		NullCheck(L_0);
		bool L_1 = AudioSource_get_isPlaying_m4213444423(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = __this->get_dragging_20();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		AudioSource_t1740077639 * L_3 = __this->get_source_18();
		AudioClip_t794140988 * L_4 = __this->get_failSound_17();
		NullCheck(L_3);
		AudioSource_PlayOneShot_m823779350(L_3, L_4, (0.5f), /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void GameManager::spawnFerry()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2091695206_MethodInfo_var;
extern const uint32_t GameManager_spawnFerry_m1563167987_MetadataUsageId;
extern "C"  void GameManager_spawnFerry_m1563167987 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_spawnFerry_m1563167987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_ferriesOnQueue_23();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		GameObject_t3674682005 * L_1 = __this->get_ferryPrefab_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_2 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		GameManager_randomizePosition_m2150844736(__this, L_3, /*hidden argument*/NULL);
		List_1_t747900261 * L_4 = __this->get_ferriesInGame_11();
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_4);
		List_1_Add_m2091695206(L_4, L_5, /*hidden argument*/List_1_Add_m2091695206_MethodInfo_var);
		int32_t L_6 = __this->get_ferriesOnQueue_23();
		__this->set_ferriesOnQueue_23(((int32_t)((int32_t)L_6-(int32_t)1)));
		return;
	}
}
// System.Void GameManager::randomizePosition(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t2212926951_m262378119_MethodInfo_var;
extern const uint32_t GameManager_randomizePosition_m2150844736_MetadataUsageId;
extern "C"  void GameManager_randomizePosition_m2150844736 (GameManager_t2369589051 * __this, GameObject_t3674682005 * ___newFerry0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_randomizePosition_m2150844736_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		GameObject_t3674682005 * L_0 = __this->get_ferrySpawn_13();
		NullCheck(L_0);
		BoxCollider2D_t2212926951 * L_1 = GameObject_GetComponent_TisBoxCollider2D_t2212926951_m262378119(L_0, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t2212926951_m262378119_MethodInfo_var);
		NullCheck(L_1);
		Bounds_t2711641849  L_2 = Collider2D_get_bounds_m1087503006(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = Bounds_get_min_m2329472069((&V_0), /*hidden argument*/NULL);
		V_3 = L_3;
		float L_4 = (&V_3)->get_x_1();
		Vector3_t4282066566  L_5 = Bounds_get_max_m2329243351((&V_0), /*hidden argument*/NULL);
		V_4 = L_5;
		float L_6 = (&V_4)->get_x_1();
		float L_7 = Random_Range_m3362417303(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t4282066566  L_8 = Bounds_get_min_m2329472069((&V_0), /*hidden argument*/NULL);
		V_5 = L_8;
		float L_9 = (&V_5)->get_y_2();
		Vector3_t4282066566  L_10 = Bounds_get_max_m2329243351((&V_0), /*hidden argument*/NULL);
		V_6 = L_10;
		float L_11 = (&V_6)->get_y_2();
		float L_12 = Random_Range_m3362417303(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		GameObject_t3674682005 * L_13 = ___newFerry0;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		float L_15 = V_1;
		float L_16 = V_2;
		GameObject_t3674682005 * L_17 = __this->get_ferrySpawn_13();
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		V_7 = L_19;
		float L_20 = (&V_7)->get_z_3();
		Vector3_t4282066566  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2926210380(&L_21, L_15, L_16, L_20, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_position_m3111394108(L_14, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::updateTimer()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1536;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t GameManager_updateTimer_m1621426954_MetadataUsageId;
extern "C"  void GameManager_updateTimer_m1621426954 (GameManager_t2369589051 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_updateTimer_m1621426954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	String_t* V_2 = NULL;
	{
		float L_0 = __this->get_gameTimer_24();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)L_0/(float)(60.0f))), /*hidden argument*/NULL);
		V_0 = (((float)((float)L_1)));
		float L_2 = __this->get_gameTimer_24();
		int32_t L_3 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, (fmodf(L_2, (60.0f))), /*hidden argument*/NULL);
		V_1 = (((float)((float)L_3)));
		float L_4 = V_1;
		if ((!(((float)L_4) == ((float)(60.0f)))))
		{
			goto IL_003c;
		}
	}
	{
		V_2 = _stringLiteral1536;
		goto IL_0066;
	}

IL_003c:
	{
		float L_5 = V_1;
		if ((!(((float)L_5) < ((float)(10.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_6 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral48, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0066;
	}

IL_005e:
	{
		String_t* L_8 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		V_2 = L_8;
	}

IL_0066:
	{
		Text_t9039225 * L_9 = __this->get_timer_9();
		String_t* L_10 = Single_ToString_m5736032((&V_0), /*hidden argument*/NULL);
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, L_10, _stringLiteral58, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_12);
		return;
	}
}
// System.Void GameManager::endGame(System.Boolean)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral297094036;
extern const uint32_t GameManager_endGame_m2581740274_MetadataUsageId;
extern "C"  void GameManager_endGame_m2581740274 (GameManager_t2369589051 * __this, bool ___win0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameManager_endGame_m2581740274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_gameOver_19((bool)1);
		int32_t L_0 = __this->get_optimalFinishTime_26();
		float L_1 = __this->get_gameTimer_24();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_0-(int32_t)L_2));
		int32_t L_3 = V_0;
		__this->set_score_22(((int32_t)((int32_t)((int32_t)1000)-(int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)10))))));
		int32_t L_4 = __this->get_score_22();
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		__this->set_score_22(0);
	}

IL_003d:
	{
		int32_t* L_5 = __this->get_address_of_score_22();
		String_t* L_6 = Int32_ToString_m1286526384(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral297094036, L_6, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputScript::.ctor()
extern "C"  void InputScript__ctor_m2173249014 (InputScript_t1346190613 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputScript::Start()
extern const MethodInfo* GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var;
extern const uint32_t InputScript_Start_m1120386806_MetadataUsageId;
extern "C"  void InputScript_Start_m1120386806 (InputScript_t1346190613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InputScript_Start_m1120386806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameManager_t2369589051 * L_1 = GameObject_GetComponent_TisGameManager_t2369589051_m494774470(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var);
		__this->set_gamemanager_4(L_1);
		return;
	}
}
// System.Void InputScript::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t InputScript_Update_m378104791_MetadataUsageId;
extern "C"  void InputScript_Update_m378104791 (InputScript_t1346190613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InputScript_Update_m378104791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t4210255029  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TouchU5BU5D_t3635654872* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameManager_t2369589051 * L_0 = __this->get_gamemanager_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_gameOver_19();
		if (L_1)
		{
			goto IL_0099;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		InputScript_castRay_m4164570143(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0043;
		}
	}
	{
		Ferry_t67768570 * L_4 = __this->get_ferry_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		InputScript_stopDrag_m3399393476(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3635654872* L_6 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		V_2 = 0;
		goto IL_008b;
	}

IL_0050:
	{
		TouchU5BU5D_t3635654872* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		V_0 = (*(Touch_t4210255029 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = Touch_get_phase_m3314549414((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)3))))
		{
			goto IL_0075;
		}
	}
	{
		InputScript_stopDrag_m3399393476(__this, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0075:
	{
		int32_t L_10 = Touch_get_phase_m3314549414((&V_0), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0087;
		}
	}
	{
		InputScript_castRay_m4164570143(__this, /*hidden argument*/NULL);
	}

IL_0087:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_12 = V_2;
		TouchU5BU5D_t3635654872* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0050;
		}
	}
	{
		goto IL_00bd;
	}

IL_0099:
	{
		Ferry_t67768570 * L_14 = __this->get_ferry_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00bd;
		}
	}
	{
		Ferry_t67768570 * L_16 = __this->get_ferry_3();
		NullCheck(L_16);
		L_16->set_dragging_2((bool)0);
		__this->set_ferry_3((Ferry_t67768570 *)NULL);
	}

IL_00bd:
	{
		return;
	}
}
// System.Void InputScript::stopDrag()
extern "C"  void InputScript_stopDrag_m3399393476 (InputScript_t1346190613 * __this, const MethodInfo* method)
{
	{
		GameManager_t2369589051 * L_0 = __this->get_gamemanager_4();
		NullCheck(L_0);
		GameManager_stopDrag_m1676044778(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputScript::castRay()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisFerry_t67768570_m132757375_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral67768570;
extern const uint32_t InputScript_castRay_m4164570143_MetadataUsageId;
extern "C"  void InputScript_castRay_m4164570143 (InputScript_t1346190613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InputScript_castRay_m4164570143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_1 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_2 = Camera_ScreenToWorldPoint_m1572306334(L_0, L_1, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_5 = Physics2D_Raycast_m1285622667(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RaycastHit2D_t1374744384  L_6 = V_0;
		bool L_7 = RaycastHit2D_op_Implicit_m3517997337(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		Transform_t1659122786 * L_8 = RaycastHit2D_get_transform_m1318597140((&V_0), /*hidden argument*/NULL);
		__this->set_hitObj_2(L_8);
		Transform_t1659122786 * L_9 = __this->get_hitObj_2();
		NullCheck(L_9);
		String_t* L_10 = Component_get_tag_m217485006(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, _stringLiteral67768570, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		Transform_t1659122786 * L_12 = __this->get_hitObj_2();
		NullCheck(L_12);
		Ferry_t67768570 * L_13 = Component_GetComponent_TisFerry_t67768570_m132757375(L_12, /*hidden argument*/Component_GetComponent_TisFerry_t67768570_m132757375_MethodInfo_var);
		__this->set_ferry_3(L_13);
		GameManager_t2369589051 * L_14 = __this->get_gamemanager_4();
		Ferry_t67768570 * L_15 = __this->get_ferry_3();
		NullCheck(L_14);
		GameManager_setFerryDragged_m3525614672(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void Map::.ctor()
extern "C"  void Map__ctor_m2539182511 (Map_t77116 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Map::Start()
extern const MethodInfo* GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2369589051;
extern const uint32_t Map_Start_m1486320303_MetadataUsageId;
extern "C"  void Map_Start_m1486320303 (Map_t77116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Map_Start_m1486320303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	Sprite_t3199167241 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	Rect_t4241904616  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Rect_t4241904616  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2369589051, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameManager_t2369589051 * L_1 = GameObject_GetComponent_TisGameManager_t2369589051_m494774470(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var);
		__this->set_gamemanager_2(L_1);
		Camera_t2727095145 * L_2 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = Camera_get_orthographicSize_m3215515490(L_2, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((double)((double)((double)((double)((double)((double)(((double)((double)L_3)))*(double)(2.0)))*(double)(((double)((double)L_4)))))/(double)(((double)((double)L_5)))));
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		SpriteRenderer_t2548470764 * L_7 = GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113(L_6, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2548470764_m3588502113_MethodInfo_var);
		NullCheck(L_7);
		Sprite_t3199167241 * L_8 = SpriteRenderer_get_sprite_m3481747968(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Sprite_t3199167241 * L_9 = V_1;
		NullCheck(L_9);
		Rect_t4241904616  L_10 = Sprite_get_textureRect_m3946160520(L_9, /*hidden argument*/NULL);
		V_8 = L_10;
		float L_11 = Rect_get_width_m2824209432((&V_8), /*hidden argument*/NULL);
		Sprite_t3199167241 * L_12 = V_1;
		NullCheck(L_12);
		float L_13 = Sprite_get_pixelsPerUnit_m2438708453(L_12, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_11/(float)L_13));
		Sprite_t3199167241 * L_14 = V_1;
		NullCheck(L_14);
		Rect_t4241904616  L_15 = Sprite_get_textureRect_m3946160520(L_14, /*hidden argument*/NULL);
		V_9 = L_15;
		float L_16 = Rect_get_height_m2154960823((&V_9), /*hidden argument*/NULL);
		Sprite_t3199167241 * L_17 = V_1;
		NullCheck(L_17);
		float L_18 = Sprite_get_pixelsPerUnit_m2438708453(L_17, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_16/(float)L_18));
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		Camera_t2727095145 * L_21 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_20, L_23, /*hidden argument*/NULL);
		V_10 = L_24;
		float L_25 = (&V_10)->get_z_3();
		V_4 = L_25;
		Camera_t2727095145 * L_26 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = V_4;
		Vector3_t4282066566  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, (0.0f), (0.0f), L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t4282066566  L_29 = Camera_ViewportToWorldPoint_m1641213412(L_26, L_28, /*hidden argument*/NULL);
		V_11 = L_29;
		float L_30 = (&V_11)->get_x_1();
		V_5 = L_30;
		GameObject_t3674682005 * L_31 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = GameObject_get_transform_m1278640159(L_31, /*hidden argument*/NULL);
		float L_33 = V_5;
		float L_34 = V_3;
		Vector3_t4282066566  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m1846874791(&L_35, L_33, ((float)((float)L_34/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_position_m3111394108(L_32, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		float L_37 = V_3;
		V_6 = ((float)((float)L_36/(float)L_37));
		Camera_t2727095145 * L_38 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		float L_39 = Camera_get_orthographicSize_m3215515490(L_38, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_39*(float)(2.0f)));
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = GameObject_get_transform_m1278640159(L_40, /*hidden argument*/NULL);
		double L_42 = V_0;
		float L_43 = V_2;
		float L_44 = V_7;
		float L_45 = V_6;
		float L_46 = V_3;
		Vector3_t4282066566  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m1846874791(&L_47, (((float)((float)((double)((double)L_42/(double)(((double)((double)L_43)))))))), ((float)((float)((float)((float)L_44*(float)L_45))/(float)L_46)), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localScale_m310756934(L_41, L_47, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_48 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_t1659122786 * L_49 = GameObject_get_transform_m1278640159(L_48, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_50 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = GameObject_get_transform_m1278640159(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t4282066566  L_52 = Transform_get_position_m2211398607(L_51, /*hidden argument*/NULL);
		V_12 = L_52;
		float L_53 = (&V_12)->get_x_1();
		float L_54 = V_7;
		GameObject_t3674682005 * L_55 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t1659122786 * L_56 = GameObject_get_transform_m1278640159(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t4282066566  L_57 = Transform_get_localScale_m3886572677(L_56, /*hidden argument*/NULL);
		V_13 = L_57;
		float L_58 = (&V_13)->get_y_2();
		Vector3_t4282066566  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Vector3__ctor_m1846874791(&L_59, L_53, ((float)((float)((float)((float)L_54/(float)(2.0f)))-(float)((float)((float)L_58*(float)(0.8f))))), /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m3111394108(L_49, L_59, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Map::Update()
extern "C"  void Map_Update_m3132108606 (Map_t77116 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Map::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Map_OnTriggerStay2D_m1674779988 (Map_t77116 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	{
		GameManager_t2369589051 * L_0 = __this->get_gamemanager_2();
		Collider2D_t1552025098 * L_1 = ___other0;
		NullCheck(L_0);
		GameManager_hitGround_m4148382315(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PassableTrigger::.ctor()
extern "C"  void PassableTrigger__ctor_m1207835614 (PassableTrigger_t3737800237 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PassableTrigger::Start()
extern Il2CppClass* PassableTrigger_t3737800237_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2369589051;
extern const uint32_t PassableTrigger_Start_m154973406_MetadataUsageId;
extern "C"  void PassableTrigger_Start_m154973406 (PassableTrigger_t3737800237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PassableTrigger_Start_m154973406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2369589051, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameManager_t2369589051 * L_1 = GameObject_GetComponent_TisGameManager_t2369589051_m494774470(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2369589051_m494774470_MethodInfo_var);
		((PassableTrigger_t3737800237_StaticFields*)PassableTrigger_t3737800237_il2cpp_TypeInfo_var->static_fields)->set_gamemanager_2(L_1);
		return;
	}
}
// System.Void PassableTrigger::Update()
extern "C"  void PassableTrigger_Update_m515060463 (PassableTrigger_t3737800237 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PassableTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* PassableTrigger_t3737800237_il2cpp_TypeInfo_var;
extern const uint32_t PassableTrigger_OnTriggerEnter2D_m151156570_MetadataUsageId;
extern "C"  void PassableTrigger_OnTriggerEnter2D_m151156570 (PassableTrigger_t3737800237 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PassableTrigger_OnTriggerEnter2D_m151156570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameManager_t2369589051 * L_0 = ((PassableTrigger_t3737800237_StaticFields*)PassableTrigger_t3737800237_il2cpp_TypeInfo_var->static_fields)->get_gamemanager_2();
		Collider2D_t1552025098 * L_1 = ___other0;
		NullCheck(L_0);
		GameManager_setPassable_m2555515383(L_0, (bool)1, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PassableTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* PassableTrigger_t3737800237_il2cpp_TypeInfo_var;
extern const uint32_t PassableTrigger_OnTriggerExit2D_m4249680392_MetadataUsageId;
extern "C"  void PassableTrigger_OnTriggerExit2D_m4249680392 (PassableTrigger_t3737800237 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PassableTrigger_OnTriggerExit2D_m4249680392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameManager_t2369589051 * L_0 = ((PassableTrigger_t3737800237_StaticFields*)PassableTrigger_t3737800237_il2cpp_TypeInfo_var->static_fields)->get_gamemanager_2();
		Collider2D_t1552025098 * L_1 = ___other0;
		NullCheck(L_0);
		GameManager_setPassable_m2555515383(L_0, (bool)0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
