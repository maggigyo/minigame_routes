﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t1743771669 * Collider2D_get_attachedRigidbody_m2908627162 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern "C"  Bounds_t2711641849  Collider2D_get_bounds_m1087503006 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Collider2D_INTERNAL_get_bounds_m3576678899 (Collider2D_t1552025098 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
