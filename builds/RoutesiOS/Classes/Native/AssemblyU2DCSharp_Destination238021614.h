﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// GameManager
struct GameManager_t2369589051;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Destination
struct  Destination_t238021614  : public MonoBehaviour_t667441552
{
public:
	// System.String Destination::name
	String_t* ___name_2;
	// UnityEngine.GameObject Destination::flag
	GameObject_t3674682005 * ___flag_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Destination_t238021614, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_flag_3() { return static_cast<int32_t>(offsetof(Destination_t238021614, ___flag_3)); }
	inline GameObject_t3674682005 * get_flag_3() const { return ___flag_3; }
	inline GameObject_t3674682005 ** get_address_of_flag_3() { return &___flag_3; }
	inline void set_flag_3(GameObject_t3674682005 * value)
	{
		___flag_3 = value;
		Il2CppCodeGenWriteBarrier(&___flag_3, value);
	}
};

struct Destination_t238021614_StaticFields
{
public:
	// GameManager Destination::gamemanager
	GameManager_t2369589051 * ___gamemanager_4;

public:
	inline static int32_t get_offset_of_gamemanager_4() { return static_cast<int32_t>(offsetof(Destination_t238021614_StaticFields, ___gamemanager_4)); }
	inline GameManager_t2369589051 * get_gamemanager_4() const { return ___gamemanager_4; }
	inline GameManager_t2369589051 ** get_address_of_gamemanager_4() { return &___gamemanager_4; }
	inline void set_gamemanager_4(GameManager_t2369589051 * value)
	{
		___gamemanager_4 = value;
		Il2CppCodeGenWriteBarrier(&___gamemanager_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
