﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Destination
struct Destination_t238021614;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void Destination::.ctor()
extern "C"  void Destination__ctor_m1358633789 (Destination_t238021614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Destination::Start()
extern "C"  void Destination_Start_m305771581 (Destination_t238021614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Destination::Update()
extern "C"  void Destination_Update_m894836592 (Destination_t238021614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Destination::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Destination_OnTriggerEnter2D_m1655381979 (Destination_t238021614 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Destination::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Destination_OnTriggerExit2D_m2912730471 (Destination_t238021614 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
