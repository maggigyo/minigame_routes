﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ferry
struct  Ferry_t67768570  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Ferry::dragging
	bool ___dragging_2;
	// System.String Ferry::destination
	String_t* ___destination_4;
	// UnityEngine.Vector3 Ferry::startPoint
	Vector3_t4282066566  ___startPoint_5;
	// System.Int32 Ferry::currentPassables
	int32_t ___currentPassables_11;

public:
	inline static int32_t get_offset_of_dragging_2() { return static_cast<int32_t>(offsetof(Ferry_t67768570, ___dragging_2)); }
	inline bool get_dragging_2() const { return ___dragging_2; }
	inline bool* get_address_of_dragging_2() { return &___dragging_2; }
	inline void set_dragging_2(bool value)
	{
		___dragging_2 = value;
	}

	inline static int32_t get_offset_of_destination_4() { return static_cast<int32_t>(offsetof(Ferry_t67768570, ___destination_4)); }
	inline String_t* get_destination_4() const { return ___destination_4; }
	inline String_t** get_address_of_destination_4() { return &___destination_4; }
	inline void set_destination_4(String_t* value)
	{
		___destination_4 = value;
		Il2CppCodeGenWriteBarrier(&___destination_4, value);
	}

	inline static int32_t get_offset_of_startPoint_5() { return static_cast<int32_t>(offsetof(Ferry_t67768570, ___startPoint_5)); }
	inline Vector3_t4282066566  get_startPoint_5() const { return ___startPoint_5; }
	inline Vector3_t4282066566 * get_address_of_startPoint_5() { return &___startPoint_5; }
	inline void set_startPoint_5(Vector3_t4282066566  value)
	{
		___startPoint_5 = value;
	}

	inline static int32_t get_offset_of_currentPassables_11() { return static_cast<int32_t>(offsetof(Ferry_t67768570, ___currentPassables_11)); }
	inline int32_t get_currentPassables_11() const { return ___currentPassables_11; }
	inline int32_t* get_address_of_currentPassables_11() { return &___currentPassables_11; }
	inline void set_currentPassables_11(int32_t value)
	{
		___currentPassables_11 = value;
	}
};

struct Ferry_t67768570_StaticFields
{
public:
	// UnityEngine.GameObject Ferry::map
	GameObject_t3674682005 * ___map_3;
	// System.Single Ferry::mapMaxX
	float ___mapMaxX_6;
	// System.Single Ferry::mapMinX
	float ___mapMinX_7;
	// System.Single Ferry::mapMaxY
	float ___mapMaxY_8;
	// System.Single Ferry::mapMinY
	float ___mapMinY_9;
	// System.Collections.Generic.List`1<System.String> Ferry::destinations
	List_1_t1375417109 * ___destinations_10;

public:
	inline static int32_t get_offset_of_map_3() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___map_3)); }
	inline GameObject_t3674682005 * get_map_3() const { return ___map_3; }
	inline GameObject_t3674682005 ** get_address_of_map_3() { return &___map_3; }
	inline void set_map_3(GameObject_t3674682005 * value)
	{
		___map_3 = value;
		Il2CppCodeGenWriteBarrier(&___map_3, value);
	}

	inline static int32_t get_offset_of_mapMaxX_6() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___mapMaxX_6)); }
	inline float get_mapMaxX_6() const { return ___mapMaxX_6; }
	inline float* get_address_of_mapMaxX_6() { return &___mapMaxX_6; }
	inline void set_mapMaxX_6(float value)
	{
		___mapMaxX_6 = value;
	}

	inline static int32_t get_offset_of_mapMinX_7() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___mapMinX_7)); }
	inline float get_mapMinX_7() const { return ___mapMinX_7; }
	inline float* get_address_of_mapMinX_7() { return &___mapMinX_7; }
	inline void set_mapMinX_7(float value)
	{
		___mapMinX_7 = value;
	}

	inline static int32_t get_offset_of_mapMaxY_8() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___mapMaxY_8)); }
	inline float get_mapMaxY_8() const { return ___mapMaxY_8; }
	inline float* get_address_of_mapMaxY_8() { return &___mapMaxY_8; }
	inline void set_mapMaxY_8(float value)
	{
		___mapMaxY_8 = value;
	}

	inline static int32_t get_offset_of_mapMinY_9() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___mapMinY_9)); }
	inline float get_mapMinY_9() const { return ___mapMinY_9; }
	inline float* get_address_of_mapMinY_9() { return &___mapMinY_9; }
	inline void set_mapMinY_9(float value)
	{
		___mapMinY_9 = value;
	}

	inline static int32_t get_offset_of_destinations_10() { return static_cast<int32_t>(offsetof(Ferry_t67768570_StaticFields, ___destinations_10)); }
	inline List_1_t1375417109 * get_destinations_10() const { return ___destinations_10; }
	inline List_1_t1375417109 ** get_address_of_destinations_10() { return &___destinations_10; }
	inline void set_destinations_10(List_1_t1375417109 * value)
	{
		___destinations_10 = value;
		Il2CppCodeGenWriteBarrier(&___destinations_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
