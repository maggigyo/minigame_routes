﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ferry
struct Ferry_t67768570;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Ferry::.ctor()
extern "C"  void Ferry__ctor_m905327025 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::.cctor()
extern "C"  void Ferry__cctor_m1813237788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::Start()
extern "C"  void Ferry_Start_m4147432113 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::Update()
extern "C"  void Ferry_Update_m4022196092 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::setColliderEnabled(System.Boolean)
extern "C"  void Ferry_setColliderEnabled_m1897876469 (Ferry_t67768570 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::startDrag()
extern "C"  void Ferry_startDrag_m906913573 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::stopDrag()
extern "C"  void Ferry_stopDrag_m677820201 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::goBack()
extern "C"  void Ferry_goBack_m3466615554 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ferry::checkDestination(System.String)
extern "C"  bool Ferry_checkDestination_m2217443485 (Ferry_t67768570 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::addPassable()
extern "C"  void Ferry_addPassable_m1051725339 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::removePassable()
extern "C"  void Ferry_removePassable_m2628931746 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ferry::noPassable()
extern "C"  bool Ferry_noPassable_m3164308331 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::resetPassable()
extern "C"  void Ferry_resetPassable_m3221425609 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::drag()
extern "C"  void Ferry_drag_m1311360391 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ferry::randomDestination()
extern "C"  String_t* Ferry_randomDestination_m4104054493 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ferry::assignFlag()
extern "C"  void Ferry_assignFlag_m2147000846 (Ferry_t67768570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
