﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// Ferry
struct Ferry_t67768570;
// GameManager
struct GameManager_t2369589051;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputScript
struct  InputScript_t1346190613  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform InputScript::hitObj
	Transform_t1659122786 * ___hitObj_2;
	// Ferry InputScript::ferry
	Ferry_t67768570 * ___ferry_3;
	// GameManager InputScript::gamemanager
	GameManager_t2369589051 * ___gamemanager_4;

public:
	inline static int32_t get_offset_of_hitObj_2() { return static_cast<int32_t>(offsetof(InputScript_t1346190613, ___hitObj_2)); }
	inline Transform_t1659122786 * get_hitObj_2() const { return ___hitObj_2; }
	inline Transform_t1659122786 ** get_address_of_hitObj_2() { return &___hitObj_2; }
	inline void set_hitObj_2(Transform_t1659122786 * value)
	{
		___hitObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___hitObj_2, value);
	}

	inline static int32_t get_offset_of_ferry_3() { return static_cast<int32_t>(offsetof(InputScript_t1346190613, ___ferry_3)); }
	inline Ferry_t67768570 * get_ferry_3() const { return ___ferry_3; }
	inline Ferry_t67768570 ** get_address_of_ferry_3() { return &___ferry_3; }
	inline void set_ferry_3(Ferry_t67768570 * value)
	{
		___ferry_3 = value;
		Il2CppCodeGenWriteBarrier(&___ferry_3, value);
	}

	inline static int32_t get_offset_of_gamemanager_4() { return static_cast<int32_t>(offsetof(InputScript_t1346190613, ___gamemanager_4)); }
	inline GameManager_t2369589051 * get_gamemanager_4() const { return ___gamemanager_4; }
	inline GameManager_t2369589051 ** get_address_of_gamemanager_4() { return &___gamemanager_4; }
	inline void set_gamemanager_4(GameManager_t2369589051 * value)
	{
		___gamemanager_4 = value;
		Il2CppCodeGenWriteBarrier(&___gamemanager_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
