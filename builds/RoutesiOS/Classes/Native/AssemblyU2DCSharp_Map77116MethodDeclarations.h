﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Map
struct Map_t77116;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void Map::.ctor()
extern "C"  void Map__ctor_m2539182511 (Map_t77116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Map::Start()
extern "C"  void Map_Start_m1486320303 (Map_t77116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Map::Update()
extern "C"  void Map_Update_m3132108606 (Map_t77116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Map::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Map_OnTriggerStay2D_m1674779988 (Map_t77116 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
